import {useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {NavLink} from 'react-router-dom';
import userContext from '../UserContext';

export default function AppNavBar() {

	// const [user, setUser] = useState(localStorage.getItem('email'))

	const {user} = useContext(userContext);

	return (
		<Navbar bg="light" expand="lg" className="p-3">
			<Navbar.Brand href="#home">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id = "basic-navbar-nav">
				<Nav> 
					<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} exact to="/courses">Courses</Nav.Link>
					{
						(user.id !== null) ? <Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link> : 
						<>
							<Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
						</>
					}
				</Nav>
			</Navbar.Collapse> 
		</Navbar>
		
	)
}