// import Courses from './pages/Courses';
import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import userContext from '../UserContext';
import swal from 'sweetalert2';


export default function CourseView() {

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const {user} = useContext(userContext);

	const {courseId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	useEffect(() => {
		console.log(courseId);

		fetch(`${ process.env.REACT_APP_API_URL }/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});
	}, [])

	const [count, setCount] = useState(0);
	const [seat, seatCount] = useState(30);

	const enroll = (courseId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				swal.fire({
					title: 'Enrolled Successfully',
					icon: 'success',
					text: `You are now a student of ${name} course`
				});

				navigate('/courses')
			}
			else {
				swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please Try Again!'
				})
			}
		});
	};

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>
							{
								(user.id !== null) ? <Button variant="primary" onClick={() => enroll(courseId)} block>Enroll</Button> : <Button as={Link} to ="/login" variant="danger" block>Login</Button>

							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
