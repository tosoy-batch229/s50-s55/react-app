import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import swal from 'sweetalert2';
import userContext from '../UserContext'; 

export default function Login() {

	const {user, setUser} = useContext(userContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false)

	console.log(email);
	console.log(password);

	useEffect(() => {
		// validation to enable the register button when all fields are populated and both password match.
		if(email !== '' && password !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	});

	function loginUser(e) {

		e.preventDefault();

		// process a fetch request to the corresponding backend API
		/*
			Syntax:
				fetch('url',{options})
				.then(res => res.json())
				.then(data => {})
		*/
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// it is a good practice to always print out the result of our fetch request to ensure that the correct information is received in our front end application.
			console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined.
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type.
			if(typeof data.access !== 'undefined') {

				localStorage.setItem('token', data.access);

				retrieveUserDetails(data.access);

				swal.fire({
					title: 'Login Successfull',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			}
			else {
				swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check your logindetails and try again!'
				})
			}

		})

		// localStorage.setItem('email', email);

		/*
			setUser({
				email: localStorage.getItem('email')
			})
		*/

		setEmail('');
		setPassword('');

		// alert('You are now logged in!')
	}

	const retrieveUserDetails = (token) => {

		// The token will be sent as part of the request's header information
    	// We put "Bearer" in front of the token to follow implementation standards for JWTs
    	fetch('http://localhost:4000/users/details', {
    		headers: {
    			Authorization: `Bearer ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {

    		console.log(data);

    		// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
    		setUser({
    			id: data._id,
    			isAdmin: data.isAdmin,
    		})
    	})
	}

	return (

		(user.id === null) ? 
		<>
			<h1> Login </h1>
			<Form onSubmit={e => loginUser(e)}>
			    <Form.Group className="mb-3" controlId="userEmail">
			    	<Form.Label>Email Address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Confirm Password" value={password} onChange={e => setPassword(e.target.value)} required/>
			    </Form.Group>
			    
			    {/* Conditional Rendering -> If the button is active, it should be clickable, else it should be unclickable*/}
			    {
			    	isActive ? <Button variant="primary" type="submit" id="submitBtn">Login</Button> : <Button variant="primary" type="submit" id="submitBtn" disabled>Login</Button>
			    }
		    </Form>
		</> : <Navigate to="/courses"/>
		
	)
}