import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {UserProvider} from './UserContext';
import './App.css';
import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Logout from './pages/Logout';
import CourseView from './components/CourseView';


// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

function App() {

	const [user, setUser] = useState({

		/*
			Syntax: 
				localStorage.getItem(propertyName)
		*/
		// email: localStorage.getItem('email')
		
		id: null,
		isAdmin: null
	});

	const unsetUser = () => {
		localStorage.clear();
	};

	// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
	useEffect(() => {
		console.log(user);
		console.log(localStorage)

	})

  	return(
  		// fragments "<> and </>" are used to act as a parent to each component
  		<UserProvider value={{user, setUser, unsetUser}}>
	  		<>
	  			<Router>
		  			{/* mounting the components and to prepare for output rendering */}
			  		<AppNavBar/>
			  		<Container>
			  			<Routes>
			  				<Route exact path="/" element={<Home/>}/>
			  				<Route exact path="/courses" element={<Courses/>}/>
			  				<Route exact path="/register" element={<Register/>}/>
			  				<Route exact path="/login" element={<Login/>}/>
			  				<Route exact path="/logout" element={<Logout/>}/>
			  				<Route exact path="/courseView/:courseId" element={<CourseView/>}/>
			  				<Route path="*" element={<Error/>}/>
			  			</Routes>
			  		</Container>
		  		</Router>
		  	</>
	  	</UserProvider>
  	);
};

export default App;